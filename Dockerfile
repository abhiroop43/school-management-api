# Build stage
FROM maven:3.9.6-eclipse-temurin-21-alpine AS build
WORKDIR /workspace/app

# set image name and tag
LABEL image.name="schoolmgmtapi"
LABEL image.tag="latest"

COPY pom.xml .
COPY src src

ARG MYSQL_DB
ARG MYSQL_USERNAME
ARG MYSQL_PASSWORD
ARG ADMIN_PASSWORD
ARG JWT_SECRET
ARG DEFAULT_PASSWORD

RUN mvn install -DskipTests

# Package stage
FROM eclipse-temurin:21-jre
VOLUME /tmp
COPY --from=build /workspace/app/target/schoolmgmtapi-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8919
ENTRYPOINT ["java","-jar","/app.jar"]